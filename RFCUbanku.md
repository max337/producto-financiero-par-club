# RFC Producto Fianciero Consultador
## _Made For UBankU_
by Sr Max

06/07/2022

![Image](./resources/images/UbankuLogo.png "Logo UBanku")


## TL;DR

El producto aprovecha las consultas a las centrales de riesgo crediticio, puedes obtener tu puntaje de riesgo gratis desde la plataforma de la super app al ser parte de la membresía club, además de que tienes una serie de tips y recomendaciones para lograr mejorar la perspectiva que las instituciones bancarias y financieras tienen de ti.  

## Motivación
En un espacio con amigos de la universidad se presenta la oportunidad de iniciar una conversación sobre como revisar o curiosear el puntaje crediticio que te otorga data crédito, no se lleva a cabo un desenlace real ya que ninguno de los presentes a pesar de tener alguna capacidad adquisitiva estaba dispuesto a pagar 17.000 COP para saber su puntaje. Pero al plantear la pregunta de si les parecería un buen producto, una membresía que tuviera una serie de beneficio y con la que además pudieras consultar tu puntaje cada vez que quisieras en un solo lugar, obteniendo una hipótesis de que tal vez al estudiante o al joven de gustaría tener algo así.

## Propuesta de implementación

> 1. Etapa de iteración y testeo: Esta etapa consta de 1 única semana, donde se suman los esfuerzos del equipo de finanzas y producto para construir, iterar y lanzar un pequeño sitio web con un nombre diferente al de nosotros, en el cual tendrán un prototipo desde donde podrá inscribir sus datos el usuario y por medio de una consulta rápida recibir una puntuación del sistema de midatacredito por medio de una API que se conecta a los servidores de dicha empresa. Cada consulta que se haga tiene un coste, por lo que idealmente tendríamos que elegir a un grupo pequeño de usuarios para las pruebas, y luego establecer una serie de preguntas para llegar a una conclusión sobre el experimento, las preguntas poder ser del tipo:

¿Cuál es tu nombre? 
¿En qué universidad estudias?
¿Cuál es tu principal preocupación financiera?
¿Tienes cuenta bancaria?
¿Usarías este producto si viniera incluida en una membresía con una serie de beneficios que te permiten ahorrar?

> 2. En la segunda etapa se crea y consolida un producto para incorporarse a la membresía, idealmente debe durar 1 solo sprint entre que se modela y despliega para su piloto, se espera que en esta etapa se respondan preguntas del tipo:

Cuales preguntas de incorporaran para usar el servicio 
Que tipo de datos serán necesarios
Cual será la ruta ideal planteada por UX para que el usuario entregue dichos datos
Luego de la consulta guardamos la información? 
¿Se espera según a esta puntuación recomendarle algún tipo de contenido al usuario para mejorar su puntaje?


## Métricas
Las métricas que se puede evaluar pueden variar dependiendo a la etapa, pero en una primera etapa las esenciales serán. 
> 1.	Numero de visitas al sitio
> 2.	Número de consultas
> 3.	Numero de usuario encuestados
> 4.	Taza de aprobación y aceptación del producto ideal. 


## Riesgos e inconvenientes.
Una de las preocupaciones es no encontrar la forma correcta de poder cubrir los gastos asociados a cada consulta, al no ser sostenible (solo en caso de una alta demanda del servicio) nos veríamos en la necesidad de aumentar el coste de la membrecía. 

Los productos que se proponen están diseñados para fortalecer la membresía, al incorporar un producto que no contribuya a esto se teme que el efecto de fortaleza se convierta en un desenfoque, una perdida de dinero y tiempo, al tener un riesgo alto lo podemos simplificar en un sprint rápido de una semana, al tener una fecha limite forzamos al equipo a iterar de forma rápida el prototipo y el lanzamiento, de esta manera reducimos el riesgo aplicable al desarrollo de un producto que los usuarios no utilizaran y mucho menos les resulta interesante. 

## Alternativas
Al ser un producto vinculado a un servicio de un tercero, antes de siquiera intentar construir sobre la hipótesis se puede trabajar directamente con la organización que suministra los datos, es decir incluirlos directamente a ellos y su sistema en un esfuerzo por encontrar aliados que puedan encargarse de dicho servicio y permitirles a ellos monetizar, generando una comisión sobre cada validación. 

## Impacto potencial y dependencias
Puede que el departamento en la primera etapa se vea saturado de solicitudes las cuales no pueden procesarse debido a que tendremos un límite de usuarios aplicables al servicio, de igual forma se aconsejaría orientar al usuario sobre el proceso para hacerlo de manera manual y un poco más largo, aunque no obtendrá el resultado de su puntaje si que puede tener una idea por medio de los diferentes reportes que pueden presentarse en los sistemas de riesgos. 

## ¿Qué impacto tiene esta decisión sobre soporte al cliente?
Puede que el departamento en la primera etapa se vea saturado de solicitudes las cuales no pueden procesarse debido a que tendremos un límite de usuarios aplicables al servicio, de igual forma se aconsejaría orientar al usuario sobre el proceso para hacerlo de manera manual y un poco más largo, aunque no obtendrá el resultado de su puntaje si que puede tener una idea por medio de los diferentes reportes que pueden presentarse en los sistemas de riesgos. 



 
## MadeFor SrMax on Ubanku

Eyes Only

**Armando Avendaño
6 Julio 2022**
